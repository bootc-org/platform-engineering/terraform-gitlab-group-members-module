
// https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group
data "gitlab_group" "target" {
  full_path = var.target_group_path
}

// https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group_membership
data "gitlab_group_membership" "target" {
  group_id = data.gitlab_group.target.id
}

locals {
  // Documented list of valid access_level values needed for validation.
  valid_access_levels = ["guest", "reporter", "developer", "maintainer", "owner"]

  // Required to ensure configured memebers don't clash with manually added members
  existing_member_usernames = toset(data.gitlab_group_membership.target.members[*].username)

  // Formulate a simple map of usernames to access levels.
  // Ignore any items in var.group_members if username matches an existing
  // (terraform unmanaged) user.  Note: This also validates the set of
  // usernames is unique (no duplicate `group_members` entries).
  username_access_levels = { for m in var.group_members : m["username"] => m["access_level"] }
}

// https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/user
data "gitlab_user" "user" {
  for_each = toset(keys(local.username_access_levels))
  username = each.key
}

// https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_membership
resource "gitlab_group_membership" "target" {
  for_each = local.username_access_levels

  group_id     = data.gitlab_group.target.id
  user_id      = data.gitlab_user.user[each.key].id
  access_level = each.value

  lifecycle {
    // Ensure all users are managed, any that are not should be imported.
    precondition {
      condition     = !contains(local.existing_member_usernames, each.key)
      error_message = <<EOT
Error: Attempt to manage previously manually added member: ${each.key}
Please remove the user or terraform import \
    ...cut...gitlab_group_membership.target["${data.gitlab_user.user[each.key].username}"] \
    ${data.gitlab_group.target.id}:${data.gitlab_user.user[each.key].id}
EOT
    }

    // Ensure only documented access level strings are referenced.
    precondition {
      condition     = contains(local.valid_access_levels, each.value)
      error_message = <<EOT
Invalid gitlab group access-level specified: ${each.value}
Value must be one of:%{for al in local.valid_access_levels} ${al}%{endfor}
EOT
    }
  }
}
