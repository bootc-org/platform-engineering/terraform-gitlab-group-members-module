variable "token" {
  type        = string
  description = "Gitlab token with API-scope access"
  sensitive   = true
  nullable    = false

  // Gitlab tokens are always at least 20 characters long, check 15 of them to be safe.
  validation {
    condition     = length(var.token) >= 15
    error_message = "The token value must be equal|longer than 15 characters."
  }
}

variable "base_url" {
  type        = string
  description = "API URL of gitlab instance to target for group management."
  nullable    = false
  default     = "https://gitlab.com/api/v4/"

  // The striing "http://a.com" is 12 characters
  validation {
    condition     = length(var.base_url) >= 12
    error_message = "The API URL must be equal|longer than 12 characters."
  }

  // Only a secure connection should ever be used
  validation {
    condition     = startswith(var.base_url, "https://")
    error_message = "The API URL must begin with https, a colon, and two forward-slashes."
  }
}

variable "target_group_path" {
  type        = string
  description = "Existing group name or full search path."
  nullable    = false

  // A valid name or search path is unlikely to be shorter.
  validation {
    condition     = length(var.target_group_path) >= 4
    error_message = "The target_group_path value must be equal|longer than 4 characters."
  }
}

variable "group_members" {
  type = list(object({
    username     = string
    access_level = string
  }))
  description = "List of maps, each specifying both a 'username' and 'access_level' key with non-empty values."
  nullable    = false
  // N/B: Validation of this would be very complex, instead preconditions are used in affected resources.
}
