# terraform-gitlab-group-members-module

IaC for [gitlab.com/bootc-org][4] group members management

## Badges

| Badge                   | Description          | Service      |
| ----------------------- | -------------------- | ------------ |
| ![Renovate][1]          | Dependencies         | Renovate     |
| ![Pre-commit][2]        | Static quality gates | pre-commit   |
| ![Devcontainer][3]      | Local DEV Env        | devcontainer |

[1]: https://img.shields.io/badge/renovate-enabled-brightgreen?logo=renovate
[2]: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit
[3]: https://img.shields.io/static/v1?label=devcontainer&message=enabled&logo=visualstudiocode&color=007ACC&logoColor=007ACC
[4]: https://gitlab.com/groups/bootc-org/-/group_members
